import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public dataForm: FormGroup;
  public chart: any;
  public dataArray: any[] = [];

  public constructor(private formBuilder: FormBuilder) { }

  public ngOnInit(): void {
    this.dataForm = this.formBuilder.group({
      x: ['', Validators.required],
      y: ['', Validators.required]
    });

    this.chart = new Chart('dispersion', {
      type: 'scatter',
      data: {
        datasets: [
          {
            label: '',
            data: [
            ],
            borderColor: '#168ede',
            backgroundColor: '#168ede',
            borderWidth: 5
          }
        ]
      },
      options: {
        legend: {
          position: 'bottom'
        }
      }
    });
  }

  public onSubmit(data: any): void {
    this.dataForm.reset();

    data.x2 = data.x * data.x;
    data.y2 = data.y * data.y;
    data.xy = data.x * data.y;
    this.dataArray.push(data);
    this.showData(data.x, data.y);
  }

  public sumar(atributo: string): number {
    let suma = 0;
    this.dataArray.forEach((data) =>
      suma = suma + data[atributo]
    );
    return suma;
  }

  public raiz(valor: number): number {
    return Math.sqrt(valor);
  }

  private showData(x: number, y: number): void {
    this.chart.data.datasets[0].data.push({ x: x, y: y });
    this.chart.update();
  }

}
